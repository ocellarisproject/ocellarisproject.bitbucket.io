
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" /><meta name="description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
    <title>Ocellaris: a mass-conserving higher-order DG FEM solver for free-surface flows &#8212; Ocellaris 2019.1.0 documentation</title>
    <link rel="stylesheet" href="_static/haiku.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="_static/ocellaris.css" />
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    <script async="async" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="canonical" href="https://www.ocellaris.org/index.html" />
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Ocellaris news" href="blog/index.html" />
<link rel="alternate" type="application/rss+xml" href="index.rss">

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Generated by https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="_static/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="_static/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="_static/favicon/favicon-16x16.png">
<link rel="manifest" href="_static/favicon/site.webmanifest">
<link rel="mask-icon" href="_static/favicon/safari-pinned-tab.svg" color="#fe8502">
<link rel="shortcut icon" href="_static/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-config" content="_static/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<!-- Open Graph protocol, http://ogp.me/ -->
<meta property="og:title" content="Ocellaris: a mass-conserving higher-order DG FEM solver for free-surface flows">
<meta property="og:description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
<meta property="og:image" content="https://www.ocellaris.org/figures/ocellaris_opengraph_1200x630.png">
<meta property="og:image:alt" content="The Ocellaris project logo, an Amphiprion Ocellaris clownfish">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">

 

  </head><body>
<div class="header" role="banner"><img class="rightlogo" src="_static/icon_128.png" alt="Logo" /><h1 class="heading"><a href="#">
            
            <span>Ocellaris DG FEM multiphase flow solver</span>
        </a>
    </h1>
    <h2 class="heading"><span style="text-transform: none">Ocellaris 2019.1.0 documentation</span></h2>
</div>
<div class="topnav" role="navigation" aria-label="top navigation">
    <p>
        
        <a href="#sec-documentation-and-user-guide">Documentation</a>
        <a href="blog/index.html">Blog</a>
        <a href="https://bitbucket.org/ocellarisproject/ocellaris">Source code</a>
        <a href="search.html">Search</a>
    </p>
</div>
<div class="content">
    
    
  <div class="section" id="about-ocellaris">
<h1>About Ocellaris<a class="headerlink" href="#about-ocellaris" title="Permalink to this headline">¶</a></h1>
<p>Ocellaris is a mass-conserving DG FEM solver for sharp-interface multiphase free-surface flows. Ocellaris can simulate water entry and exit of objects in ocean waves with accurate capturing of the force on the object and the behaviour of the free surface. Some examples of what Ocellaris can do, including videos of the results, are shown in the <a class="reference external" href="https://www.ocellaris.org/blog/">Ocellaris Blog</a>. Ocellaris is named after the <a class="reference external" href="https://en.wikipedia.org/wiki/Ocellaris_clownfish">Amphiprion Ocellaris</a> clownfish and is released under the Apache License, version 2.0.</p>
<div class="figure align-center" id="id1">
<a class="reference internal image-reference" href="https://www.ocellaris.org/figures/cylinder_in_waves.jpg"><img alt="Picture a cylinder in waves from an Ocellaris simulation" src="https://www.ocellaris.org/figures/cylinder_in_waves.jpg" style="width: 50%;" /></a>
<p class="caption"><span class="caption-text">Cylinder in waves; from Ocellaris via <a class="reference external" href="https://www.paraview.org/">Paraview</a> and <a class="reference external" href="https://www.blender.org/">Blender</a>.</span><a class="headerlink" href="#id1" title="Permalink to this image">¶</a></p>
</div>
<p>Ocellaris is implemented in Python and C++ with <a class="reference external" href="https://fenicsproject.org/">FEniCS</a> as the backend for the mesh and finite element assembly. <a class="reference external" href="https://www.mcs.anl.gov/petsc/">PETSc</a> is used for solving the resulting linear systems. The code is developed in Python and C++ on <a class="reference external" href="https://bitbucket.org/ocellarisproject/ocellaris">Bitbucket</a> by use of the Git version control system. If you want to contribute to Ocellaris, please read <a class="reference external" href="https://www.ocellaris.org/programmers_guide/guidelines.html">the guide to contributing</a>. Ocellaris is automatically tested on <a class="reference external" href="https://circleci.com/bb/ocellarisproject/ocellaris/tree/master">CircleCI</a> and the current CI build status is <a class="reference external" href="https://circleci.com/bb/ocellarisproject/ocellaris"><img alt="circleci_status" src="https://circleci.com/bb/ocellarisproject/ocellaris.svg?style=svg" /></a>.</p>
</div>
<div class="section" id="documentation-and-user-guide">
<span id="sec-documentation-and-user-guide"></span><h1>Documentation and user guide<a class="headerlink" href="#documentation-and-user-guide" title="Permalink to this headline">¶</a></h1>
<p>For help installing/running Ocellaris, please use the <a class="reference external" href="https://bitbucket.org/ocellarisproject/ocellaris/issues">issue tracker</a> and select <code class="docutils literal notranslate"><span class="pre">Component</span> <span class="pre">=</span> <span class="pre">User</span> <span class="pre">help</span></code>. Please read this user guide first. We may start a user forum in the future if there are sufficient requests (let us know). Also, please let us know if there documentation is unclear (or wrong) in certain areas, or if you would like to see more documentation on a specific topic. This documentation will not replace a basic course in PDEs or CFD, but if you have taken those courses we hope you will be able to use Ocellaris for your purposes without any prior exposure to DG FEM.</p>
<div class="toctree-wrapper compound">
<ul>
<li class="toctree-l1"><a class="reference internal" href="blog/index.html">Ocellaris release notes and project news blog</a></li>
<li class="toctree-l1"><a class="reference internal" href="community_guidelines.html">Community guidelines</a><ul>
<li class="toctree-l2"><a class="reference internal" href="community_guidelines.html#how-to-report-a-bug">How to report a bug</a></li>
<li class="toctree-l2"><a class="reference internal" href="community_guidelines.html#how-to-get-tech-support">How to get tech support</a></li>
<li class="toctree-l2"><a class="reference internal" href="community_guidelines.html#how-to-contribute-a-patch">How to contribute a patch</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="user_guide/user_guide.html">User guide</a><ul>
<li class="toctree-l2"><a class="reference internal" href="user_guide/install.html">Installing Ocellaris</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/install.html#dependencies">Dependencies</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/install.html#installation-using-containers">Installation using containers</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/install.html#installation-using-pip">Installation using pip</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/install.html#verifying-the-installation">Verifying the installation</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/run.html">Running Ocellaris</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/run.html#running-a-simulation">Running a simulation</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/run.html#running-a-simulation-on-multiple-cpus-with-mpi">Running a simulation on multiple CPUS with MPI</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/run.html#restart-files">Restart files</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/run.html#graphical-user-interface">Graphical user interface</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/run.html#controlling-a-running-simulation">Controlling a running simulation</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/input/input.html">Writing input files</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/input/input.html#getting-started">Getting started</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/input/input.html#file-format">File format</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/input/input.html#header">Header</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/input/input.html#input-file-sections">Input file sections</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/demos.html">Demos</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/demos.html#flow-around-a-clownfish">Flow around a clownfish</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/demos.html#dam-break">Dam break</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/demos.html#taylor-green">Taylor-Green</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/inspector.html">Ocellaris Inspector</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/inspector.html#getting-started">Getting started</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/inspector.html#opening-files">Opening files</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/inspector.html#scripting">Scripting</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/scripts.html">Utility scripts</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/scripts.html#orun-py-run-ocellaris-on-a-hpc-with-automatic-restarts">orun.py - Run Ocellaris on a HPC with automatic restarts</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/scripts.html#merge-xdmf-timeseries-py-join-multiple-xdmf-files-into-a-single-time-history">merge_xdmf_timeseries.py - join multiple XDMF files into a single time history</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/scripts.html#others">Others</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="user_guide/advanced_topics.html">Advanced topics</a><ul>
<li class="toctree-l3"><a class="reference internal" href="user_guide/advanced_topics.html#writing-a-custom-solver">Writing a custom solver</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/advanced_topics.html#writing-a-custom-boundary-condition">Writing a custom boundary condition</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/advanced_topics.html#writing-a-custom-known-field">Writing a custom known field</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/advanced_topics.html#writing-a-custom-multiphase-model">Writing a custom multiphase model</a></li>
<li class="toctree-l3"><a class="reference internal" href="user_guide/advanced_topics.html#writing-other-custom-parts">Writing other custom parts</a></li>
</ul>
</li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="programmers_guide/programmers_guide.html">Programmers guide</a><ul>
<li class="toctree-l2"><a class="reference internal" href="programmers_guide/intro.html">An introduction to the code base</a></li>
<li class="toctree-l2"><a class="reference internal" href="programmers_guide/guidelines.html">Development guidelines</a><ul>
<li class="toctree-l3"><a class="reference internal" href="programmers_guide/guidelines.html#git-workflow">Git workflow</a></li>
<li class="toctree-l3"><a class="reference internal" href="programmers_guide/guidelines.html#code-style">Code style</a></li>
<li class="toctree-l3"><a class="reference internal" href="programmers_guide/guidelines.html#automated-tests">Automated tests</a></li>
<li class="toctree-l3"><a class="reference internal" href="programmers_guide/guidelines.html#merging-your-code">Merging your code</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="programmers_guide/scripting.html">Scripting Ocellaris</a></li>
<li class="toctree-l2"><a class="reference internal" href="programmers_guide/interactive_console.html">Interactive console</a></li>
<li class="toctree-l2"><a class="reference internal" href="programmers_guide/simulation_api.html">Simulation classes API</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="license.html">License of Ocellaris</a></li>
<li class="toctree-l1"><a class="reference internal" href="contributors.html">Contributors</a></li>
<li class="toctree-l1"><a class="reference internal" href="citing.html">Citing Ocellaris</a></li>
</ul>
</div>
<p>This documentation is written by Tormod Landet and the <a class="reference internal" href="contributors.html"><span class="doc">Ocellaris contributors</span></a> and is under the same <a class="reference internal" href="license.html"><span class="doc">license</span></a> as the rest of the Ocellaris source code repository contents. See <a class="reference internal" href="citing.html#sec-citing"><span class="std std-ref">Citing Ocellaris</span></a> for how to reference Ocellaris in your research.</p>
</div>


</div>
<div class="bottomnav" role="navigation" aria-label="bottom navigation">
    
        <p>
        <a class="uplink" href="#">Contents</a>
        &#160;&#160;::&#160;&#160;
        <a href="blog/index.html">Ocellaris news</a>&#160;&#160;»
        </p>

</div>


    <div class="footer" role="contentinfo">
        &#169; Copyright 2015-2019, Tormod Landet.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.2.1.
    </div>
  </body>
</html>