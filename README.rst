Ocellaris web pages
===================

The normal web browsing version of the Ocellaris project `web pages can be
found here <https://ocellarisproject.bitbucket.io/>`_. This repository
contains the HTML source code and images used in the web pages.

The ``index.html`` file and the ``figures/`` directory in this repository can
be changed by commiting directly to the repository. **DO NOT** change files
in the ``ocellaris/`` directory, it is deleted and rebuilt by the Ocellaris
documentation builder that runs on CicleCI, see `the script that does this
here <https://bitbucket.org/ocellarisproject/ocellaris/src/master/documentation/circleci.sh>`_.
