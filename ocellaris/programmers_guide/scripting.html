
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" /><meta name="description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
    <title>Scripting Ocellaris &#8212; Ocellaris 2019.1.0 documentation</title>
    <link rel="stylesheet" href="../_static/haiku.css" type="text/css" />
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../_static/ocellaris.css" />
    <script type="text/javascript" id="documentation_options" data-url_root="../" src="../_static/documentation_options.js"></script>
    <script type="text/javascript" src="../_static/jquery.js"></script>
    <script type="text/javascript" src="../_static/underscore.js"></script>
    <script type="text/javascript" src="../_static/doctools.js"></script>
    <script type="text/javascript" src="../_static/language_data.js"></script>
    <script async="async" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="canonical" href="https://www.ocellaris.org/programmers_guide/scripting.html" />
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Interactive console" href="interactive_console.html" />
    <link rel="prev" title="Development guidelines" href="guidelines.html" />
<link rel="alternate" type="application/rss+xml" href="../index.rss">

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Generated by https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="../_static/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../_static/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../_static/favicon/favicon-16x16.png">
<link rel="manifest" href="../_static/favicon/site.webmanifest">
<link rel="mask-icon" href="../_static/favicon/safari-pinned-tab.svg" color="#fe8502">
<link rel="shortcut icon" href="../_static/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-config" content="../_static/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<!-- Open Graph protocol, http://ogp.me/ -->
<meta property="og:title" content="Scripting Ocellaris">
<meta property="og:description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
<meta property="og:image" content="https://www.ocellaris.org/figures/ocellaris_opengraph_1200x630.png">
<meta property="og:image:alt" content="The Ocellaris project logo, an Amphiprion Ocellaris clownfish">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">

 

  </head><body>
<div class="header" role="banner"><img class="rightlogo" src="../_static/icon_128.png" alt="Logo" /><h1 class="heading"><a href="../index.html">
            
            <span>Ocellaris DG FEM multiphase flow solver</span>
        </a>
    </h1>
    <h2 class="heading"><span style="text-transform: none">Ocellaris 2019.1.0 documentation</span></h2>
</div>
<div class="topnav" role="navigation" aria-label="top navigation">
    <p>
        
        <a href="../index.html#sec-documentation-and-user-guide">Documentation</a>
        <a href="../blog/index.html">Blog</a>
        <a href="https://bitbucket.org/ocellarisproject/ocellaris">Source code</a>
        <a href="../search.html">Search</a>
    </p>
</div>
<div class="content">
    
    
  <div class="section" id="scripting-ocellaris">
<span id="id1"></span><h1>Scripting Ocellaris<a class="headerlink" href="#scripting-ocellaris" title="Permalink to this headline">¶</a></h1>
<p>There are three main ways of scripting Ocellaris:</p>
<ol class="arabic simple">
<li><p>Generate input files by a script and run Ocellaris on these</p></li>
<li><p>Include Python code in the input file (see the <a class="reference internal" href="../user_guide/user_guide.html#user-guide"><span class="std std-ref">User Guide</span></a> for details)</p></li>
<li><p>Use the Python programming interface</p></li>
</ol>
<p>A brief example of the first would be something like this that runs Ocellaris
with two different time steps by producing input files and starting Ocellaris
as a command line application:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">yaml</span><span class="o">,</span> <span class="nn">subprocess</span>

<span class="c1"># Load the template input file</span>
<span class="k">with</span> <span class="nb">open</span><span class="p">(</span><span class="s1">&#39;template.inp&#39;</span><span class="p">,</span> <span class="s1">&#39;rt&#39;</span><span class="p">)</span> <span class="k">as</span> <span class="n">inp</span><span class="p">:</span>
    <span class="n">input_dict</span> <span class="o">=</span> <span class="n">yaml</span><span class="o">.</span><span class="n">load</span><span class="p">(</span><span class="n">inp</span><span class="p">)</span>

<span class="k">for</span> <span class="n">dt</span> <span class="ow">in</span> <span class="p">[</span><span class="mf">0.1</span><span class="p">,</span> <span class="mf">0.05</span><span class="p">]:</span>
    <span class="c1"># Modify the input file</span>
    <span class="n">prefix</span> <span class="o">=</span> <span class="s1">&#39;test_dt_</span><span class="si">%.3f</span><span class="s1">&#39;</span> <span class="o">%</span> <span class="n">dt</span>
    <span class="n">input_dict</span><span class="p">[</span><span class="s1">&#39;time&#39;</span><span class="p">][</span><span class="s1">&#39;dt&#39;</span><span class="p">]</span> <span class="o">=</span> <span class="n">dt</span>
    <span class="n">input_dict</span><span class="p">[</span><span class="s1">&#39;output&#39;</span><span class="p">][</span><span class="s1">&#39;prefix&#39;</span><span class="p">]</span> <span class="o">=</span> <span class="n">prefix</span>

    <span class="c1"># Save the modified input file</span>
    <span class="n">new_inp_file</span> <span class="o">=</span> <span class="n">prefix</span> <span class="o">+</span> <span class="s1">&#39;.inp&#39;</span>
    <span class="k">with</span> <span class="nb">open</span><span class="p">(</span><span class="n">new_inp_file</span><span class="p">,</span> <span class="s1">&#39;wt&#39;</span><span class="p">)</span> <span class="k">as</span> <span class="n">out</span><span class="p">:</span>
        <span class="n">yaml</span><span class="o">.</span><span class="n">dump</span><span class="p">(</span><span class="n">input_dict</span><span class="p">,</span> <span class="n">out</span><span class="p">)</span>

    <span class="c1"># Run Ocellaris with the modified input file</span>
    <span class="n">p</span> <span class="o">=</span> <span class="n">subprocess</span><span class="o">.</span><span class="n">Popen</span><span class="p">([</span><span class="s1">&#39;python&#39;</span><span class="p">,</span> <span class="s1">&#39;-m&#39;</span><span class="p">,</span> <span class="s1">&#39;ocellaris&#39;</span><span class="p">,</span> <span class="n">new_inp_file</span><span class="p">],</span>
                         <span class="n">stdout</span><span class="o">=</span><span class="n">subprocess</span><span class="o">.</span><span class="n">PIPE</span><span class="p">,</span> <span class="n">stderr</span><span class="o">=</span><span class="n">subprocess</span><span class="o">.</span><span class="n">PIPE</span><span class="p">)</span>
    <span class="n">stdout</span><span class="p">,</span> <span class="n">stderr</span> <span class="o">=</span> <span class="n">p</span><span class="o">.</span><span class="n">communicate</span><span class="p">()</span>
</pre></div>
</div>
<p>The same can be accomplished from a script which uses the Ocellaris Python API:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">from</span> <span class="nn">ocellaris</span> <span class="kn">import</span> <span class="n">Simulation</span><span class="p">,</span> <span class="n">setup_simulation</span><span class="p">,</span> <span class="n">run_simulation</span>

<span class="k">for</span> <span class="n">dt</span> <span class="ow">in</span> <span class="p">[</span><span class="mf">0.1</span><span class="p">,</span> <span class="mf">0.05</span><span class="p">]:</span>
    <span class="n">prefix</span> <span class="o">=</span> <span class="s1">&#39;test_dt_</span><span class="si">%.3f</span><span class="s1">&#39;</span> <span class="o">%</span> <span class="n">dt</span>

    <span class="c1"># Create a simulation object, load an input file and modify the time step</span>
    <span class="n">sim</span> <span class="o">=</span> <span class="n">Simulation</span><span class="p">()</span>
    <span class="n">sim</span><span class="o">.</span><span class="n">input</span><span class="o">.</span><span class="n">read_yaml</span><span class="p">(</span><span class="s1">&#39;template.inp&#39;</span><span class="p">)</span>
    <span class="n">sim</span><span class="o">.</span><span class="n">input</span><span class="o">.</span><span class="n">set_value</span><span class="p">(</span><span class="s1">&#39;time/dt&#39;</span><span class="p">,</span> <span class="n">dt</span><span class="p">)</span>
    <span class="n">sim</span><span class="o">.</span><span class="n">input</span><span class="o">.</span><span class="n">set_value</span><span class="p">(</span><span class="s1">&#39;output/prefix&#39;</span><span class="p">,</span> <span class="n">prefix</span><span class="p">)</span>

    <span class="c1"># Run Ocellaris</span>
    <span class="n">setup_simulation</span><span class="p">(</span><span class="n">sim</span><span class="p">)</span>
    <span class="n">run_simulation</span><span class="p">(</span><span class="n">sim</span><span class="p">)</span>
</pre></div>
</div>
<p>For more information about what you can do with the simulation object, see the
<a class="reference internal" href="simulation_api.html#simulation-api"><span class="std std-ref">Simulation classes API</span></a> documentation.</p>
<p>Examples of this can be seen in the convergence scripts which can be found in
the <code class="docutils literal notranslate"><span class="pre">cases/</span></code> subdirectory of the Ocellaris repository.</p>
</div>


</div>
<div class="bottomnav" role="navigation" aria-label="bottom navigation">
    
        <p>
        «&#160;&#160;<a href="guidelines.html">Development guidelines</a>
        &#160;&#160;::&#160;&#160;
        <a class="uplink" href="../index.html">Contents</a>
        &#160;&#160;::&#160;&#160;
        <a href="interactive_console.html">Interactive console</a>&#160;&#160;»
        </p>

</div>


    <div class="footer" role="contentinfo">
        &#169; Copyright 2015-2019, Tormod Landet.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.2.1.
    </div>
  </body>
</html>