
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" /><meta name="description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
    <title>Probes &#8212; Ocellaris 2019.1.0 documentation</title>
    <link rel="stylesheet" href="../../_static/haiku.css" type="text/css" />
    <link rel="stylesheet" href="../../_static/pygments.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../_static/ocellaris.css" />
    <script type="text/javascript" id="documentation_options" data-url_root="../../" src="../../_static/documentation_options.js"></script>
    <script type="text/javascript" src="../../_static/jquery.js"></script>
    <script type="text/javascript" src="../../_static/underscore.js"></script>
    <script type="text/javascript" src="../../_static/doctools.js"></script>
    <script type="text/javascript" src="../../_static/language_data.js"></script>
    <script async="async" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <link rel="canonical" href="https://www.ocellaris.org/user_guide/input/probes.html" />
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Hooks - run custom code at given times during a simulation" href="hooks.html" />
    <link rel="prev" title="Output control" href="output.html" />
<link rel="alternate" type="application/rss+xml" href="../../index.rss">

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Generated by https://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="180x180" href="../../_static/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="../../_static/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="../../_static/favicon/favicon-16x16.png">
<link rel="manifest" href="../../_static/favicon/site.webmanifest">
<link rel="mask-icon" href="../../_static/favicon/safari-pinned-tab.svg" color="#fe8502">
<link rel="shortcut icon" href="../../_static/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-config" content="../../_static/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<!-- Open Graph protocol, http://ogp.me/ -->
<meta property="og:title" content="Probes">
<meta property="og:description" content="The Ocellaris user guide, documentation and news blog shows how to use the Ocellaris higher order DG FEM Navier-Stokes solver to simulate free surface flows such as the air/water flow in breaking ocean waves">
<meta property="og:image" content="https://www.ocellaris.org/figures/ocellaris_opengraph_1200x630.png">
<meta property="og:image:alt" content="The Ocellaris project logo, an Amphiprion Ocellaris clownfish">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">

 

  </head><body>
<div class="header" role="banner"><img class="rightlogo" src="../../_static/icon_128.png" alt="Logo" /><h1 class="heading"><a href="../../index.html">
            
            <span>Ocellaris DG FEM multiphase flow solver</span>
        </a>
    </h1>
    <h2 class="heading"><span style="text-transform: none">Ocellaris 2019.1.0 documentation</span></h2>
</div>
<div class="topnav" role="navigation" aria-label="top navigation">
    <p>
        
        <a href="../../index.html#sec-documentation-and-user-guide">Documentation</a>
        <a href="../../blog/index.html">Blog</a>
        <a href="https://bitbucket.org/ocellarisproject/ocellaris">Source code</a>
        <a href="../../search.html">Search</a>
    </p>
</div>
<div class="content">
    
    
  <div class="section" id="probes">
<span id="inp-probes"></span><h1>Probes<a class="headerlink" href="#probes" title="Permalink to this headline">¶</a></h1>
<p>Probes can be used to ease postprocessing and to extract numbers directly from
the simulation instead of having to write XDMF files which are large and may
be somewhat interpolated from the “real” value inside the simulation. Often
XDMF and probes are used at the same time, probes write their values every time
step and XDMF may be written every 100 time steps for disk saving purposes.</p>
<p>The probes list contains dictionaries describing each probe. An example can be
seen below under <a class="reference internal" href="#inp-probes-pointprobe"><span class="std std-ref">PointProbe</span></a>. Some common configuration
options for all the probes are:</p>
<dl class="describe">
<dt>
<code class="sig-name descname">name</code></dt>
<dd><p>Name of the probe, often used in the name of the output file</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">type</code></dt>
<dd><p>The probe type. One of</p>
<ul class="simple">
<li><p>IsoSurface</p></li>
<li><p>LineProbe</p></li>
<li><p>PlaneProbe</p></li>
<li><p>PointProbe</p></li>
</ul>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">enabled</code></dt>
<dd><p>Default on, you can turn of a probe if you want</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">file_name</code></dt>
<dd><p>Where to save the data. Normally a default value based on the name is used</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">write_interval</code></dt>
<dd><p>How often to write the probe results, default 1, every time step</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">custom_hook</code></dt>
<dd><p>When to run the probe. Normally run at the end oe each time step, but you
can give the name of a hook here instead (typically
<code class="docutils literal notranslate"><span class="pre">MultiPhaseModelUpdated</span></code>). Not all probes support this.</p>
</dd></dl>

<div class="section" id="isosurface">
<h2>IsoSurface<a class="headerlink" href="#isosurface" title="Permalink to this headline">¶</a></h2>
<p>Only implemented in 2D for now. Outputs the position of an ISO surface. Can be
used to save the free surface. Another way (for 3D) is to use XDMF output and
create a Contour in Paraview as a post-processing step.</p>
<dl class="describe">
<dt>
<code class="sig-name descname">field</code></dt>
<dd><p>Name of the field to study, e.g., <code class="docutils literal notranslate"><span class="pre">c</span></code></p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">value</code></dt>
<dd><p>The value of the field at the ISO surface, e.g. <code class="docutils literal notranslate"><span class="pre">0.5</span></code> for the free
surface in a VOF simulation</p>
</dd></dl>

</div>
<div class="section" id="lineprobe">
<h2>LineProbe<a class="headerlink" href="#lineprobe" title="Permalink to this headline">¶</a></h2>
<dl class="describe">
<dt>
<code class="sig-name descname">field</code></dt>
<dd><p>Name of the field to study, e.g., <code class="docutils literal notranslate"><span class="pre">u0</span></code></p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">startpos</code></dt>
<dd><p>A list of numbers, the coordinates of a point in the domain</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">endpos</code></dt>
<dd><p>A list of numbers, the coordinates of a point in the domain</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">Npoints</code></dt>
<dd><p>Number of probing points along the line segment from startpos to endpos</p>
</dd></dl>

</div>
<div class="section" id="planeprobe">
<h2>PlaneProbe<a class="headerlink" href="#planeprobe" title="Permalink to this headline">¶</a></h2>
<p>Saves an XDMF plot file of the specified field intersected by a plane.
Sometimes it can be useful to have 2D slices of 3D simulations since the 2D
slices are smaller in size and can be written more often without too much IO.</p>
<dl class="describe">
<dt>
<code class="sig-name descname">field</code></dt>
<dd><p>Name of the field to study, e.g., <code class="docutils literal notranslate"><span class="pre">u0</span></code>. You can also give a list,
<code class="docutils literal notranslate"><span class="pre">[u0,</span> <span class="pre">u1,</span> <span class="pre">u2]</span></code>, but the functions in the list must share the same
function space (most likely DG2 in this case)</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">plane_point</code></dt>
<dd><p>A list of numbers, the coordinates of a point on the plane</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">plane_normal</code></dt>
<dd><p>A list of numbers, the normal direction of the plane</p>
</dd></dl>

<dl class="describe">
<dt>
<code class="sig-name descname">xlim, ylim, zlim</code></dt>
<dd><p>Lists of two numbers specifying limits to the extents of the plane. By
default the plane is as large as the intersection with the 3D mesh allows.</p>
</dd></dl>

</div>
<div class="section" id="pointprobe">
<span id="inp-probes-pointprobe"></span><h2>PointProbe<a class="headerlink" href="#pointprobe" title="Permalink to this headline">¶</a></h2>
<p>Probe one or more fields in given points</p>
<dl class="describe">
<dt>
<code class="sig-name descname">probe_points</code></dt>
<dd><p>A list of function names and the coordinates of the points to probe. The
name of each probe must also be given so that you can figure out which
value belongs to which point. See example below for the syntax</p>
</dd></dl>

<div class="highlight-yaml notranslate"><div class="highlight"><pre><span></span><span class="nt">probes</span><span class="p">:</span>
    <span class="p p-Indicator">-</span>   <span class="nt">name</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">pressure_probes</span>
        <span class="nt">enabled</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">yes</span>
        <span class="nt">type</span><span class="p">:</span> <span class="l l-Scalar l-Scalar-Plain">PointProbe</span>
        <span class="nt">probe_points</span><span class="p">:</span>
        <span class="p p-Indicator">-</span>   <span class="p p-Indicator">[</span><span class="s">&#39;p&#39;</span><span class="p p-Indicator">,</span> <span class="s">&#39;probe1&#39;</span><span class="p p-Indicator">,</span> <span class="nv">-1</span><span class="p p-Indicator">,</span> <span class="nv">0.5</span><span class="p p-Indicator">,</span> <span class="nv">0.3</span><span class="p p-Indicator">]</span>
        <span class="p p-Indicator">-</span>   <span class="l l-Scalar l-Scalar-Plain">py$ [&#39;p&#39;, &#39;probe2&#39;, L - 2, 1e-3, 1e-3]</span>
        <span class="p p-Indicator">-</span>   <span class="l l-Scalar l-Scalar-Plain">py$ [&#39;c&#39;, &#39;cprobe&#39;, L - 2, 1e-3, 1e-3]</span>
</pre></div>
</div>
</div>
</div>


</div>
<div class="bottomnav" role="navigation" aria-label="bottom navigation">
    
        <p>
        «&#160;&#160;<a href="output.html">Output control</a>
        &#160;&#160;::&#160;&#160;
        <a class="uplink" href="../../index.html">Contents</a>
        &#160;&#160;::&#160;&#160;
        <a href="hooks.html">Hooks - run custom code at given times during a simulation</a>&#160;&#160;»
        </p>

</div>


    <div class="footer" role="contentinfo">
        &#169; Copyright 2015-2019, Tormod Landet.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.2.1.
    </div>
  </body>
</html>